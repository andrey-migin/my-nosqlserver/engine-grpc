using System.ServiceModel;
using System.Threading.Tasks;
using MyNoSqlServer.Engine.Grpc.Contracts;

namespace MyNoSqlServer.Engine.Grpc
{
    
    [ServiceContract(Name = "Sync")]
    public interface ISyncApi
    {
        [OperationContract(Action = "StartSession")]
        ValueTask<StartSyncSessionGrpcResponse> StartSessionAsync(StartSyncSessionGrpcRequest request);
        
        [OperationContract(Action = "SyncTable")]
        ValueTask<SyncTableGrpcResponse> SyncTableAsync(SyncTableGrpcRequest request);
    }
    
}