using System;
using System.Runtime.Serialization;
using MyNoSqlServer.Abstractions;

namespace MyNoSqlServer.Engine.Grpc.Models
{
    [DataContract]
    public class RowGrpcModel : IMyNoSqlDbEntity
    {
        
        [DataMember(Order = 1)]
        public string PartitionKey { get; set; }
        
        [DataMember(Order = 2)] 
        public string RowKey { get; set; }
        
        [DataMember(Order = 3)]
        public EntityContentType ContentType { get; set; }
        
        [DataMember(Order = 4)]
        public DateTime Expires { get; set; }
        
        [DataMember(Order = 5)]
        public DateTime TimeStamp { get; set; }
        
        [DataMember(Order = 6)]
        public byte[] Data { get; set; }


        public static RowGrpcModel Create(IMyNoSqlDbEntity src)
        {
            return new RowGrpcModel
            {
                PartitionKey = src.PartitionKey,
                RowKey = src.RowKey,
                Data = src.Data,
                Expires = src.Expires,
                ContentType = src.ContentType,
                TimeStamp = src.TimeStamp
            };
        }
        
    }
}