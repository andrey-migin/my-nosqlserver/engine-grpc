using System.ServiceModel;
using System.Threading.Tasks;
using MyNoSqlServer.Engine.Grpc.Contracts;

namespace MyNoSqlServer.Engine.Grpc
{
    
    [ServiceContract(Name = "WriteData")]
    public interface IWriteDataApi
    {
        
        [OperationContract(Action = "Insert")]
        ValueTask<GrpcResponse> InsertAsync(InsertEntityGrpcRequest request);

        [OperationContract(Action = "InsertOrReplace")]
        ValueTask<GrpcResponse> InsertOrReplaceAsync(InsertEntityGrpcRequest request);

        
        [OperationContract(Action = "BulkInsertOrReplace")]
        ValueTask<GrpcResponse> BulkInsertOrReplaceAsync(InsertEntitiesGrpcRequest request);

        
        [OperationContract(Action = "Delete")]
        ValueTask<GrpcResponse> DeleteAsync(DeleteRecordGrpcRequest request);

        [OperationContract(Action = "CleanAndKeepLastRecords")]
        ValueTask<GetDataGrpcResponse> CleanAndKeepLastRecordsAsync(CleanAndKeepLastRecordsGrpcRequest request);

        [OperationContract(Action = "CleanTableAndBulkInsert")]
        ValueTask<GrpcResponse> CleanTableAndBulkInsertAsync(CleanAndBulkInsertGrpcRequest request);
        
        [OperationContract(Action = "CleanPartitionsAndBulkInsert")]
        ValueTask<GrpcResponse> CleanPartitionsAndBulkInsertAsync(CleanAndBulkInsertGrpcRequest request);

    }
}