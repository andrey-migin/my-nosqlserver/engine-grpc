using System.ServiceModel;
using System.Threading.Tasks;
using MyNoSqlServer.Engine.Grpc.Contracts;

namespace MyNoSqlServer.Engine.Grpc
{
    [ServiceContract(Name = "ReadData")]
    public interface IReadDataApi
    {
        [OperationContract(Action = "GetRows")]
        ValueTask<GetDataGrpcResponse> GetRowsAsync(GetRowGrpcRequest request);

        [OperationContract(Action = "SinglePartitionMultipleRows")]
        ValueTask<GetDataGrpcResponse> SinglePartitionMultipleRowsAsync(
            GetSinglePartitionMultipleRowsGrpcRequest request);


        ValueTask<GetDataGrpcResponse> HighestRowAndBelow(
            HighestRowAndBelowGrpcRequest request);
        
        [OperationContract(Action = "Count")]
        ValueTask<GetCountGrpcResponse> GetCount(GetCountGrpcRequest request);
        
        
        
        
        
        
        
    }
    
}