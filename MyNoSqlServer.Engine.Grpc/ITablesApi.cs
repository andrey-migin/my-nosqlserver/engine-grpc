using System.Collections.Generic;
using System.ServiceModel;
using System.Threading.Tasks;
using MyNoSqlServer.Engine.Grpc.Contracts;

namespace MyNoSqlServer.Engine.Grpc
{
    
    [ServiceContract(Name = "Tables")]
    public interface ITablesApi
    {

        [OperationContract(Action = "List")]
        ValueTask<IEnumerable<string>> GetListAsync();

        [OperationContract(Action = "CreateIfNotExists")]
        ValueTask<GrpcResponse> CreateIfNotExistsAsync(CreateTableGrpcContract request);
        
        [OperationContract(Action = "Create")]
        ValueTask<GrpcResponse> CreateAsync(CreateTableGrpcContract request);
        
        [OperationContract(Action = "Clean")]
        ValueTask<GrpcResponse> CleanAsync(CleanTableGrpcContract request);

    }
}