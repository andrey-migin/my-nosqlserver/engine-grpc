using System.Runtime.Serialization;
using MyNoSqlServer.Abstractions;

namespace MyNoSqlServer.Engine.Grpc.Contracts
{
    
    [DataContract]
    public class DeleteRecordGrpcRequest : ICommonRowRequestContract
    {
        [DataMember(Order = 1)]
        public string TableName { get; set; }

        [DataMember(Order = 1)]
        public string PartitionKey { get; set; }

        [DataMember(Order = 2)]
        public string RowKey { get; set; }

        [DataMember(Order = 3)]
        public DataSynchronizationPeriod SyncPeriod { get; set; }

        
    }
}