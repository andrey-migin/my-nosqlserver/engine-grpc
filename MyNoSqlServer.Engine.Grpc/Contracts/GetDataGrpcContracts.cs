using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using MyNoSqlServer.Engine.Grpc.Models;

namespace MyNoSqlServer.Engine.Grpc.Contracts
{

    [DataContract]
    public class GetRowGrpcRequest
    {
        [DataMember(Order = 1)]
        public string TableName { get; set; }
        
        [DataMember(Order = 2)]
        public string PartitionKey { get; set; }
        
        [DataMember(Order = 3)]
        public string RowKey { get; set; }        

        [DataMember(Order = 4)]
        public int? Limit { get; set; }        
        
        [DataMember(Order = 5)]
        public int? Skip { get; set; }        
        
    }
    
    [DataContract]
    public class GetDataGrpcResponse
    {
        
        [DataMember(Order = 1)]
        public GrpcRequestResult Result { get; set; }
        
        [DataMember(Order = 2)]
        public IEnumerable<RowGrpcModel> Entities { get; set; }

        public static GetDataGrpcResponse CreateFail(GrpcRequestResult result)
        {
            return new GetDataGrpcResponse
            {
                 Result = result
            };
        }
        
        public static GetDataGrpcResponse CreateOk(IEnumerable<RowGrpcModel> entities)
        {
            return new GetDataGrpcResponse
            {
                Result = GrpcRequestResult.Ok,
                Entities = entities
            };
        }

        
        public static GetDataGrpcResponse EmptyResponse = new GetDataGrpcResponse
        {
            Result = GrpcRequestResult.Ok,
            Entities = Array.Empty<RowGrpcModel>()
        };
        
    }
}