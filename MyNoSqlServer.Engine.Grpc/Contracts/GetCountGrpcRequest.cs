using System.Runtime.Serialization;

namespace MyNoSqlServer.Engine.Grpc.Contracts
{
    [DataContract]
    public class GetCountGrpcRequest
    {
        [DataMember(Order = 1)]
        public string TableName { get; set; }
        
        [DataMember(Order = 2)]
        public string PartitionKey { get; set; }
    }
    
    [DataContract]
    public class GetCountGrpcResponse
    {
        [DataMember(Order = 1)]
        public GrpcRequestResult Result { get; set; }
        
        [DataMember(Order = 2)]
        public int Count { get; set; }

        public static readonly GetCountGrpcResponse TableNameIsNotSpecifiedInstance = new GetCountGrpcResponse
        {
            Result = GrpcRequestResult.TableNameIsNotSpecified
        };
        
        public static readonly GetCountGrpcResponse TableNotFoundInstance = new GetCountGrpcResponse
        {
            Result = GrpcRequestResult.TableNotFound
        };

    }
}