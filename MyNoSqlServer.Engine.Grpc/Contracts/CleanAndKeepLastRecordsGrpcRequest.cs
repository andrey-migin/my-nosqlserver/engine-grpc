using System.Runtime.Serialization;
using MyNoSqlServer.Abstractions;

namespace MyNoSqlServer.Engine.Grpc.Contracts
{
    [DataContract]
    public class CleanAndKeepLastRecordsGrpcRequest
    {
        
        [DataMember(Order = 1)]
        public string TableName { get; set; }
        
        [DataMember(Order = 2)]
        public DataSynchronizationPeriod SyncPeriod { get; set; }
        
        [DataMember(Order = 3)]
        public string PartitionKey { get; set; }

        [DataMember(Order = 4)]
        public int Amount { get; set; }

    }
}