using System.Runtime.Serialization;

namespace MyNoSqlServer.Engine.Grpc.Contracts
{

    public enum GrpcRequestResult
    {
        Ok, 
        IsShuttingDown, 
        TableNameIsNotSpecified, 
        Conflict, 
        TableNotFound,
        RowNotFound, 
        PartitionKeyIsEmpty, 
        RowKeyIsEmpty,
        ImmediatePersistentIsNotSupported
    }
    
    [DataContract]
    public class GrpcResponse
    {
        [DataMember(Order = 1)]
        public GrpcRequestResult Result { get; set; }

        public static readonly GrpcResponse OkInstance = new GrpcResponse
        {
            Result = GrpcRequestResult.Ok
        };
        
        public static readonly GrpcResponse ShuttingDownInstance = new GrpcResponse
        {
            Result = GrpcRequestResult.IsShuttingDown
        };
        
        public static readonly GrpcResponse TableNameIsNotSpecifiedInstance = new GrpcResponse
        {
            Result = GrpcRequestResult.TableNameIsNotSpecified
        };
        
        public static readonly GrpcResponse ImmediatePersistentIsNotSupportedInstance = new GrpcResponse
        {
            Result = GrpcRequestResult.ImmediatePersistentIsNotSupported
        };

        
        public static readonly GrpcResponse ConflictInstance = new GrpcResponse
        {
            Result = GrpcRequestResult.Conflict
        };
        
        public static readonly GrpcResponse TableNotFoundInstance = new GrpcResponse
        {
            Result = GrpcRequestResult.TableNotFound
        };
        
        public static readonly GrpcResponse PartitionKeyIsEmptyInstance = new GrpcResponse
        {
            Result = GrpcRequestResult.PartitionKeyIsEmpty
        };
        
        public static readonly GrpcResponse RowKeyIsEmptyInstance = new GrpcResponse
        {
            Result = GrpcRequestResult.RowKeyIsEmpty
        };
        
        public static readonly GrpcResponse RowNotFoundInstance = new GrpcResponse
        {
            Result = GrpcRequestResult.RowNotFound
        };
        
    }
}