using System.Runtime.Serialization;

namespace MyNoSqlServer.Engine.Grpc.Contracts
{
    [DataContract]
    public class HighestRowAndBelowGrpcRequest
    {
        
        [DataMember(Order = 1)]
        public string TableName { get; set; }

        [DataMember(Order = 2)]
        public string PartitionKey { get; set; }

        [DataMember(Order = 3)]
        public string RowKey { get; set; }

        [DataMember(Order = 4)]
        public int MaxAmount { get; set; }

    }
}