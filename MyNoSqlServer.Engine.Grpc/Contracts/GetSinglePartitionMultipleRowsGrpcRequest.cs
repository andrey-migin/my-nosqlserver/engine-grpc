using System.Collections.Generic;
using System.Runtime.Serialization;

namespace MyNoSqlServer.Engine.Grpc.Contracts
{
    [DataContract]
    public class GetSinglePartitionMultipleRowsGrpcRequest
    {
        
        [DataMember(Order = 1)]
        public string TableName { get; set; }
        
        [DataMember(Order = 2)]
        public string PartitionKey { get; set; }
        
        [DataMember(Order = 3)]
        public IEnumerable<string> RowKeys { get; set; }
    }
    
}