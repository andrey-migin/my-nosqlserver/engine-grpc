using System.Runtime.Serialization;

namespace MyNoSqlServer.Engine.Grpc.Contracts
{
    
    [DataContract]
    public class StartSyncSessionGrpcRequest
    {
        [DataMember(Order = 1)]
        public string ClientName { get; set; }
    }


    [DataContract]
    public class StartSyncSessionGrpcResponse
    {
        [DataMember(Order = 1)]        
        public string SessionId { get; set; }
    }
}