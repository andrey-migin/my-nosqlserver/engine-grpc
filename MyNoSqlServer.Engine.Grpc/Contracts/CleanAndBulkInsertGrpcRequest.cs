using System.Collections.Generic;
using System.Runtime.Serialization;
using MyNoSqlServer.Abstractions;
using MyNoSqlServer.Engine.Grpc.Models;

namespace MyNoSqlServer.Engine.Grpc.Contracts
{
    
    [DataContract]
    public class CleanAndBulkInsertGrpcRequest
    {
        
        [DataMember(Order = 1)]
        public string TableName { get; set; }
        
        [DataMember(Order = 2)]
        public DataSynchronizationPeriod SyncPeriod { get; set; }
        
        [DataMember(Order = 3)]
        public IEnumerable<RowGrpcModel> Entities { get; set; }
    }
}