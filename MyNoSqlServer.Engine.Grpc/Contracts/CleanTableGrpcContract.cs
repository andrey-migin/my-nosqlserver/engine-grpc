
using System.Runtime.Serialization;
using MyNoSqlServer.Abstractions;

namespace MyNoSqlServer.Engine.Grpc.Contracts
{
    

    
    [DataContract]
    public class CleanTableGrpcContract
    {
        [DataMember(Order = 1)]
        public string TableName { get; set; }
        
        [DataMember(Order = 2)]
        public DataSynchronizationPeriod SyncPeriod { get; set; }
    }
}