using System.Runtime.Serialization;

namespace MyNoSqlServer.Engine.Grpc.Contracts
{
    [DataContract]
    public class CreateTableGrpcContract
    {
        [DataMember(Order = 1)]
        public string TableName { get; set; }
    }
}