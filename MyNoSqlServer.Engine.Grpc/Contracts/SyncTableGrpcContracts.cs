using System.Collections.Generic;
using System.Runtime.Serialization;
using MyNoSqlServer.Engine.Grpc.Models;

namespace MyNoSqlServer.Engine.Grpc.Contracts
{
    [DataContract]
    public class SyncTableGrpcRequest
    {
        [DataMember(Order = 1)]        
        public string SessionId { get; set; }
        
        [DataMember(Order = 2)]        
        public string TableId { get; set; }
    }

    [DataContract]
    public class SyncTableGrpcResponse
    {
        [DataMember(Order = 1)]
        public bool SessionIsExpired { get; set; }
        
        [DataMember(Order = 2)]
        public SyncTableGrpcContract SyncTable { get; set; }
        
        [DataMember(Order = 3)]
        public SyncPartitionGrpcContract SyncPartition { get; set; }
        
        [DataMember(Order = 4)]
        public UpdateRowsGrpcContract UpdateRows { get; set; }

        [DataMember(Order = 5)]
        public DeleteRowsGrpcContract DeleteRows { get; set; }
        
    }
    
    [DataContract]
    public class SyncTableGrpcContract
    {
        [DataMember(Order = 1)]
        public bool SyncTable { get; set; }
        [DataMember(Order = 2)]
        public string PartitionKey { get; set; }
        [DataMember(Order = 3)]
        public IEnumerable<RowGrpcModel> Rows { get; set; } 
    }
    
    [DataContract]
    public class SyncPartitionGrpcContract
    {
        [DataMember(Order = 1)]
        public bool SyncPartition { get; set; }
        [DataMember(Order = 2)]
        public string PartitionKey { get; set; }
        [DataMember(Order = 3)]
        public IEnumerable<RowGrpcModel> Rows { get; set; } 
    }
    
    [DataContract]
    public class UpdateRowsGrpcContract
    {
        [DataMember(Order = 1)]
        public bool UpdateRows { get; set; }
        [DataMember(Order = 2)]
        public IEnumerable<RowGrpcModel> Rows { get; set; } 
    }

    [DataContract]
    public class DeleteRowsGrpcContract
    {
        [DataMember(Order = 1)]
        public bool DeleteRows { get; set; }
        [DataMember(Order = 2)]
        public IEnumerable<RowGrpcModel> Rows { get; set; } 
        
    }

    [DataContract]
    public class DeleteRowGrpcContract
    {
        [DataMember(Order = 1)]
        public string PartitionKey { get; set; }
        
        [DataMember(Order = 2)]
        public string RowKey { get; set; }
    }

}