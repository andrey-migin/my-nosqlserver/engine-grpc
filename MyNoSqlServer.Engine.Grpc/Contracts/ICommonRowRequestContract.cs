namespace MyNoSqlServer.Engine.Grpc.Contracts
{
    public interface ICommonRowRequestContract
    {
        
        string TableName { get; }
        string PartitionKey { get; }
        string RowKey { get; }
        
    }
}